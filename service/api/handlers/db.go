package handlers

import (
	"context"
	"service/pkg/db"
)

type DB interface {
	CreateServer(ctx context.Context, server *db.Server)
	DeleteServer(ctx context.Context, serverId, userId uint32)
	GetServers(ctx context.Context, userId uint32)
	CreateUser(ctx context.Context, user *db.User)
	GetUser(ctx context.Context, email string)
}
