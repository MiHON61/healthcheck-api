package handlers

import (
	"net/mail"
	"service/api/proto"
	"service/pkg/db"
	"service/pkg/tokenizer"

	"context"

	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *Server) Registration(ctx context.Context, req *proto.RegistrationRequest) (*proto.RegistrationResponse, error) {
	err := req.Validate()
	if err != nil {
		return nil, err
	}

	email := req.GetEmail()
	password := req.GetPassword()

	_, err = mail.ParseAddress(email)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "invalid email")
	}
	passwordBytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)

	user, err := s.Db.CreateUser(ctx, &db.User{Email: email, Password: string(passwordBytes)})
	if err != nil {
		return nil, err
	}
	token, err := tokenizer.GenerateToken(user.Id, key)
	if err != nil {
		log.Info(err)
		return nil, err
	}
	return &proto.RegistrationResponse{Token: token}, nil
}

func (s *Server) Auth(ctx context.Context, req *proto.AuthRequest) (*proto.AuthResponse, error) {
	err := req.Validate()
	if err != nil {
		return nil, err
	}

	email := req.GetEmail()
	password := req.GetPassword()

	user, err := s.Db.GetUser(ctx, email)
	if err != nil {
		return nil, status.Error(codes.Internal, "internal error")
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return nil, err
	}
	token, err := tokenizer.GenerateToken(user.Id, key)
	if err != nil {
		log.Info(err)
		return nil, err
	}
	return &proto.AuthResponse{Token: token}, nil
}
