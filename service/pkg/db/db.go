package db

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/op/go-logging"
)

var (
	log = logging.MustGetLogger("main")
)

type DB struct {
	Db *sqlx.DB
}

// Init инициализация бд
func Init(dbHost, dbName, dbUser, dbPassword string) *DB {

	db := NewDB(dbHost, dbName, dbUser, dbPassword)

	return &DB{Db: db}

}
func (db *DB) Close() {
	if err := db.Db.Close(); err != nil {
		log.Error("Error while closing db")
	}
}

func NewDB(host, dbName, user, pass string) *sqlx.DB {
	dsn := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable",
		user,
		pass,
		host,
		dbName)

	postgres, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		log.Panic(err.Error())
	}

	postgres.SetMaxOpenConns(25)
	postgres.SetMaxIdleConns(25)
	postgres.SetConnMaxLifetime(5 * time.Minute)
	return postgres
}
