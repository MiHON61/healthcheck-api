module service

go 1.17

require (
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/envoyproxy/protoc-gen-validate v0.6.7
	github.com/go-redis/redis/v8 v8.11.5
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.10.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.4
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/genproto v0.0.0-20220323144105-ec3c684e5b14
	google.golang.org/grpc v1.45.0
	google.golang.org/protobuf v1.28.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
)
