package tokenizer

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go/v4"
	"github.com/op/go-logging"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var log = logging.MustGetLogger("authApi")

type Claims struct {
	jwt.StandardClaims
	Id uint32 `json:"id"`
}

func GenerateToken(id uint32, key *ecdsa.PrivateKey) (string, error) {

	token := jwt.NewWithClaims(jwt.SigningMethodES256, Claims{StandardClaims: jwt.StandardClaims{
		ExpiresAt: jwt.At(time.Now().Add(2 * time.Hour)),
		IssuedAt:  jwt.At(time.Now()),
	},
		Id: id,
	})

	return token.SignedString(key)
}
func ParseUserToken(ctx context.Context, key *ecdsa.PrivateKey) (uint32, error) {
	// получаем токен из headers
	accessToken, err := getTokenFromContext(ctx)
	if err != nil {
		return 0, err
	}
	log.Info(accessToken)
	// парсим токен и проверяем на валидность
	email, err := checkToken(accessToken, key)
	log.Info(err)
	return email, err
}

func getTokenFromContext(ctx context.Context) (accessToken string, err error) {
	authorizationHeader, err := parseHeaderData(ctx, "authorization")
	if err != nil {
		err = status.Error(codes.InvalidArgument, "invalid token")
		return
	}
	accessToken = authorizationHeader[0]
	if len(accessToken) == 0 {
		err = status.Error(codes.InvalidArgument, "invalid token")
		return
	}
	return
}
func parseHeaderData(ctx context.Context, key string) (result []string, err error) {
	headers, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		err = status.Error(codes.InvalidArgument, "invalid token")
		return
	}
	if headers.Len() == 0 || len(headers.Get(key)) == 0 {
		err = status.Error(codes.InvalidArgument, "invalid token")
		return
	}
	result = headers.Get(key)
	return
}

func checkToken(tok string, key *ecdsa.PrivateKey) (uint32, error) {
	fmt.Println(key)
	token, err := jwt.ParseWithClaims(tok, &Claims{}, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodECDSA); !ok {

			return "", status.Error(codes.InvalidArgument, "invalid token")
		}
		return key, nil
	})

	if err != nil {
		log.Info(err)
		return 0, err
	}

	if claims, ok := token.Claims.(*Claims); ok && token.Valid {
		log.Info(claims)
		return claims.Id, nil
	}
	return 0, nil
}
