package handlers

import (
	"net/http"
	"service/api/proto"
	"service/pkg/db"
	"service/pkg/tokenizer"
	"sync"

	"context"
)

func (s *Server) AddServer(ctx context.Context, req *proto.AddServerRequest) (*proto.AddServerResponse, error) {
	err := req.Validate()
	if err != nil {
		return nil, err
	}

	address := req.GetAddress()
	userId, err := tokenizer.ParseUserToken(ctx, key)
	if err != nil {
		log.Info(err)
		return nil, err
	}
	server := &db.Server{
		UserId:  userId,
		Address: address,
	}
	err = s.Db.CreateServer(ctx, server)
	if err != nil {
		return nil, err
	}

	return &proto.AddServerResponse{Id: server.Id, Address: server.Address}, nil
}

func (s *Server) RemoveServer(ctx context.Context, req *proto.RemoveServerRequest) (*proto.Empty, error) {
	serverId := req.GetId()

	userId, err := tokenizer.ParseUserToken(ctx, key)
	if err != nil {
		log.Info(err)
		return nil, err
	}

	err = s.Db.DeleteServer(ctx, serverId, userId)
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func (s *Server) PingServers(ctx context.Context, req *proto.Empty) (*proto.PingServersResponse, error) {

	userId, err := tokenizer.ParseUserToken(ctx, key)
	if err != nil {
		log.Info(err)
		return nil, err
	}

	servers, err := s.Db.GetServers(ctx, userId)
	if err != nil {
		return nil, err
	}

	workingSites := make([]*proto.PingServersResponse_ServerStatus, 0, len(servers))
	notWorkingSites := make([]*proto.PingServersResponse_ServerStatus, 0, len(servers))
	var wg sync.WaitGroup
	wg.Add(len(servers))
	for _, server := range servers {
		go func() {
			defer wg.Done()
			r, err := http.Get(server.Address)
			if err != nil {
				notWorkingSites = append(notWorkingSites, &proto.PingServersResponse_ServerStatus{Address: server.Address, Status: uint32(r.StatusCode)})
			} else {
				workingSites = append(workingSites, &proto.PingServersResponse_ServerStatus{Address: server.Address, Status: uint32(r.StatusCode)})
			}
		}()
	}
	wg.Wait()
	return &proto.PingServersResponse{WorkingSites: workingSites, NotWorkingSites: notWorkingSites}, nil
}
