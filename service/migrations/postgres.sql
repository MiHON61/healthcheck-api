CREATE TABLE IF NOT EXISTS users
(
    id               serial       not null,
    email            varchar(255) not null unique,
    password         varchar(255) not null,

    primary key (id)
);

CREATE TABLE IF NOT EXISTS servers
(
    id          serial           not null,
    user_id     integer          not null,
	address     varchar(255)     not null unique,

    constraint fk_inv_user_id
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE,
	
	primary key (id)

);
