package main

import (
	"os"
	"os/signal"
	"service/api/handlers"
	"service/pkg/db"
	"syscall"

	"github.com/joho/godotenv"
	"github.com/op/go-logging"
)

var (
	log = logging.MustGetLogger("main")
)

type Conf struct {
	DbHost     string
	DbName     string
	DbUser     string
	DbPassword string
	ApiHost    string
}

func main() {
	config := getConfig()

	database := db.Init(config.DbHost, config.DbName, config.DbUser, config.DbPassword)
	go handlers.Start(config.ApiHost, database)

	defer database.Close()

	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	_ = <-c

	log.Info("Gracefully shutting down...")

}

func getConfig() *Conf {

	// load .env file
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")

	}
	config := &Conf{
		DbHost:     os.Getenv("DBHOST"),
		DbName:     os.Getenv("DBNAME"),
		DbUser:     os.Getenv("DBUSER"),
		DbPassword: os.Getenv("DBPASSWORD"),
		ApiHost:    os.Getenv("HOST"),
	}

	return config
}
