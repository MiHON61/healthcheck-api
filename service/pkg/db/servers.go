package db

import (
	"context"
)

type Server struct {
	Id      uint32 `db:"id" json:"id,omitempty"`
	Address string `db:"address" json:"address,omitempty"`
	UserId  uint32 `db:"user_id" json:"user_id,omitempty"`
}

func (database *DB) CreateServer(ctx context.Context, server *Server) error {

	stmt, err := database.Db.PrepareNamedContext(ctx, "INSERT INTO servers (address, user_id) VALUES(:address, :user_id) RETURNING *")
	if err != nil {
		return err
	}

	if err = stmt.GetContext(ctx, server, server); err != nil {
		return err
	}

	return nil
}

func (database *DB) DeleteServer(ctx context.Context, serverId, userId uint32) error {

	if _, err := database.Db.ExecContext(ctx, "DELETE FROM servers WHERE id=$1 AND user_id=$2", serverId, userId); err != nil {
		return err
	}

	return nil
}

func (database *DB) GetServers(ctx context.Context, userId uint32) (servers []*Server, err error) {

	err = database.Db.GetContext(ctx, servers, "SELECT * FROM servers WHERE user_id=$1", userId)
	return
}
