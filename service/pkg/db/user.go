package db

import (
	"context"
)

type User struct {
	Id       uint32 `db:"id" json:"id,omitempty"`
	Email    string `db:"email" json:"email,omitempty"`
	Password string `db:"password" json:"password,omitempty"`
}

func (database *DB) CreateUser(ctx context.Context, user *User) (*User, error) {

	stmt, err := database.Db.PrepareNamedContext(ctx, "INSERT INTO users (email, password) VALUES(:email, :password) RETURNING *")
	if err != nil {
		return nil, err
	}

	if err = stmt.GetContext(ctx, user, user); err != nil {
		return nil, err
	}

	return user, nil
}

func (database *DB) GetUser(ctx context.Context, email string) (*User, error) {
	result := User{}
	err := database.Db.GetContext(ctx, &result, "SELECT * FROM users WHERE email=$1", email)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
