package handlers

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"net/http"
	"service/api/proto"
	"service/pkg/db"

	grpcRuntime "github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/op/go-logging"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
)

type Server struct {
	proto.UnimplementedApiServiceServer
	Db *db.DB
}

var log = logging.MustGetLogger("authApi")
var key *ecdsa.PrivateKey

func Start(host string, database *db.DB) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	key, _ = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	apiServer := &Server{Db: database}
	mux := grpcRuntime.NewServeMux(
		grpcRuntime.WithMarshalerOption(grpcRuntime.MIMEWildcard, &grpcRuntime.JSONPb{MarshalOptions: protojson.MarshalOptions{
			EmitUnpopulated: true,
		}}),
		grpcRuntime.WithRoutingErrorHandler(routingErrorsHandler),
		grpcRuntime.WithErrorHandler(errorHandler),
	)

	log.Infof("Listening gateway %s", host)
	if err := proto.RegisterApiServiceHandlerServer(ctx, mux, apiServer); err != nil {
		log.Fatal(err)
		return
	}

	go log.Fatal(http.ListenAndServe(host, mux))
}

func errorHandler(ctx context.Context, mux *grpcRuntime.ServeMux, marshaller grpcRuntime.Marshaler, w http.ResponseWriter, r *http.Request, err error) {
	var result error
	s := status.Convert(err)
	if s.Code() == 2 || s.Code() == 13 {
		log.Error(err.Error())
	}
	result = s.Err()
	grpcRuntime.DefaultHTTPErrorHandler(ctx, mux, marshaller, w, r, result)
}

func routingErrorsHandler(ctx context.Context, mux *grpcRuntime.ServeMux, m grpcRuntime.Marshaler, w http.ResponseWriter, r *http.Request, statusCode int) {
	sterr := status.Error(codes.Internal, "internal error")
	switch statusCode {
	case http.StatusBadRequest:
		sterr = status.Error(codes.InvalidArgument, "bad request")
	case http.StatusMethodNotAllowed:
		sterr = status.Error(codes.Unimplemented, "method not allowed")
	case http.StatusNotFound:
		sterr = status.Error(codes.NotFound, "not found")
	}
	grpcRuntime.HTTPError(ctx, mux, m, w, r, sterr)
}
